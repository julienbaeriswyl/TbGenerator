#!/bin/sh

SCRIPT=`realpath -s $0`
SCRIPTPATH=`dirname $SCRIPT`

$SCRIPTPATH/gen_tb_vhdl/simple_tb/test_gen_simple_vhdl_tb.sh
$SCRIPTPATH/gen_assertions_sv/std_sv_assertions/test_gen_std_sv_assertions.sh
$SCRIPTPATH/gen_tb_sv/simple_sv_tb/test_gen_simple_sv_tb.sh
$SCRIPTPATH/gen_assertions_psl/std_psl_assertions/test_gen_std_psl_assertions.sh
