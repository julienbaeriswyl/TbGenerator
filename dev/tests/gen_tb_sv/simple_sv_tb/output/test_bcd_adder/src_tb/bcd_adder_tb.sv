/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : bcd_adder_tb.sv
Author   : TbGenerator
Date     : 06.03.2019

Context  :

********************************************************************************
Description : This module is a simple SystemVerilog testbench.
              It instanciates the DUV and proposes a testcase function.

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   06.03.2019  TbGen      Initial version

*******************************************************************************/


// The wrapper has the same interface as the component to verify
module bcd_adder_tb #(int ERRNO = 0, int NDIGITS = 4, int TESTCASE = 0)();

    // Instantiation of the DUV
    bcd_adder#(ERRNO, NDIGITS) duv(.*);

    TODO__TypeNotCasted input0_i;
    TODO__TypeNotCasted input1_i;
    TODO__TypeNotCasted result_o;
    logic[TODO__uncasted_left_member:0] hamming_o;
    


    // clock generation
    always #5 TODO__clock_notfound = ~TODO__clock_notfound;

    // Clocking block
    default clocking cb @(posedge TODO__clock_notfound);
        output #3ns TODO__reset_notfound,
               input0_i = input0_i,
               input1_i = input1_i;
        input 
               result_o = result_o,
               hamming_o = hamming_o;
    endclocking
    


    task testcase0();
        $display("Let's start first test case");
        // TODO : assign default values to inputs

        // Let's reset the system
        cb.TODO__reset_notfound <= 1;
        ##1 cb.TODO__reset_notfound <= 0;

        // Let's wait a bit
        ##10;

        repeat (10) begin
            // Let's generate some stimuli
            // TODO : generate some stimuli

            ##1;
        end
    endtask



    // Programme lancé au démarrage de la simulation
    program TestSuite;
        initial begin
            if (testcase == 0)
                testcase0();
            else
                $display("Ach, test case not yet implemented");
            $display("done!");
            $stop;
        end
    endprogram

endmodule
