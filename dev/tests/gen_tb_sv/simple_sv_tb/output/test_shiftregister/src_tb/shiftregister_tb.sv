/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : shiftregister_tb.sv
Author   : TbGenerator
Date     : 06.03.2019

Context  :

********************************************************************************
Description : This module is a simple SystemVerilog testbench.
              It instanciates the DUV and proposes a testcase function.

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   06.03.2019  TbGen      Initial version

*******************************************************************************/


// The wrapper has the same interface as the component to verify
module shiftregister_tb #(int DATASIZE = 8, int TESTCASE = 0)();

    // Instantiation of the DUV
    shiftregister#(DATASIZE) duv(.*);

    logic clk_i = 0;
    logic rst_i;
    logic[1:0] mode_i;
    logic[DATASIZE - 1:0] load_value_i;
    logic ser_in_msb_i;
    logic ser_in_lsb_i;
    logic[DATASIZE - 1:0] value_o;
    


    // clock generation
    always #5 clk_i = ~clk_i;

    // Clocking block
    default clocking cb @(posedge clk_i);
        output #3ns rst_i,
               mode_i = mode_i,
               load_value_i = load_value_i,
               ser_in_msb_i = ser_in_msb_i,
               ser_in_lsb_i = ser_in_lsb_i;
        input 
               value_o = value_o;
    endclocking
    


    task testcase0();
        $display("Let's start first test case");
        // TODO : assign default values to inputs

        // Let's reset the system
        cb.rst_i <= 1;
        ##1 cb.rst_i <= 0;

        // Let's wait a bit
        ##10;

        repeat (10) begin
            // Let's generate some stimuli
            // TODO : generate some stimuli

            ##1;
        end
    endtask



    // Programme lancé au démarrage de la simulation
    program TestSuite;
        initial begin
            if (testcase == 0)
                testcase0();
            else
                $display("Ach, test case not yet implemented");
            $display("done!");
            $stop;
        end
    endprogram

endmodule
