import hdlConvertor

import sys
sys.path.append('../../src/parsers')

import pprint
from vhdlentityparser import *


res = parseFile("../src_vhd/bcd_adder.vhd")


entity = getEntity(res)

pp = pprint.PrettyPrinter(depth=12)
pp.pprint(entity)

for gen in getGenerics(entity):
    print(getName(gen))
    print(getGenericType(gen))
    print(getGenericValue(gen))

for port in getPorts(entity):
    print(getPortName(port))
    print(getPortDirection(port))
    t = getPortType(port)
    if (isTypeLogic(t)):
        print("is std_logic")
    if (isTypeLogicVector(t)):
        print("is std_logic_vector")
        r = getRange(t)
        if (isValid(r)):
            print('{0} {1} {2}'.format(r['rLeft'], r['dir'], r['rRight']))
        else:
            print('uncasted range')
