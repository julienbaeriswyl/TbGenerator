-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : timer_top.vhd
--
-- Description  :
--
-- Auteur       : Etienne Messerli
-- Date         : 28.10.2015
-- Version      : 0.0
--
-- Utilise      : Manipulation Timer pour cours CSN
--
--| Modifications |------------------------------------------------------------
-- Ver   Auteur Date               Description
-- 0.0    EMI   29.09.2014   version intiale, entite du timer_top
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity timer_top is
port(
        clock_i     : in  std_logic;
        nReset_i    : in  std_logic;
        Mono_nDiv_i : in  std_logic;
        en_div_i    : in  std_logic;
        run_mono_i  : in  std_logic;
        val_i       : in  std_logic_vector(6 downto 0);
        done_o      : out std_logic
);
end timer_top;


architecture behave of alu is

	signal a_s: std_logic_vector(6 downto 0);
	signal b_s: std_logic_vector(6 downto 0);
	signal s_s: std_logic_vector(6 downto 0);

begin

    done_o  <= '0';

end behave;
