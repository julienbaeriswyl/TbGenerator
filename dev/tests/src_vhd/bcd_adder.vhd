-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- Institut REDS
--
-- Composant      : bcd_adder
-- Description    : A BDC adder.
-- Auteur         : Yann Thoma
-- Date           : 01.03.2017
-- Version        : 1.0
--
-- Modification : -
--
-----------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.math_real.all;
use work.bcd_pkg.all;
use work.ConfigurationPackage.all;

entity bcd_adder is
generic (
        ERRNO : integer := 0;
        NDIGITS : integer := 4
);
port (
    input0_i  : in  bcd_number(NDIGITS - 1 downto 0);
    input1_i  : in  bcd_number(NDIGITS - 1 downto 0);
    result_o  : out bcd_number(NDIGITS downto 0);
    hamming_o : out std_logic_vector(integer(ceil(log2(real(4*NDIGITS)))) downto 0)
);
end bcd_adder;

architecture behave of bcd_adder is

begin

    result_o  <= (others => '0');
    hamming_o <= (others => '0');

end behave;
