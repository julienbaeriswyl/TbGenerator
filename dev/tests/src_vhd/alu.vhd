-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- Institut REDS
--
-- Composant    : alu
-- Description  : An arithmetic and logic unit
-- Auteur       : Yann Thoma
-- Date         : 28.02.2012
-- Version      : 1.0
--
-----------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is
generic (
    SIZE  : integer := 8;
    ERRNO : integer := 0
);
port (
    a_i    : in  std_logic_vector(SIZE-1 downto 0);
    b_i    : in  std_logic_vector(SIZE-1 downto 0);
    s_o    : out std_logic_vector(SIZE-1 downto 0);
    c_o    : out std_logic;
    mode_i : in  std_logic_vector(2 downto 0)
);
end alu;


architecture behave of alu is

begin

    s_o <= (others => '0');
    c_o <= '0';

end behave;
