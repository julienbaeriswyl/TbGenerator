-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- Institut REDS
--
-- Composant      : shiftregister
-- Description    : A simple shift register. Action depends on the mode:
--                  00 => hold
--                  01 => shift left
--                  10 => shift right
--                  11 => loadAdditionneur BCD.
--
-- Auteur         : Yann Thoma
-- Date           : 03.11.2017
-- Version        : 1.0
--
-- Modification : -
--
-----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity shiftregister is
generic (DATASIZE : integer := 8
);
port (
    clk_i        : in  std_logic;
    rst_i        : in  std_logic;
    mode_i       : in  std_logic_vector(1 downto 0);
    load_value_i : in  std_logic_vector(DATASIZE-1 downto 0);
    ser_in_msb_i : in  std_logic;
    ser_in_lsb_i : in  std_logic;
    value_o      : out std_logic_vector(DATASIZE-1 downto 0)
);
end shiftregister;

architecture behave of shiftregister is

begin

    value_o  <= (others => '0');

end behave;
