
proc compile_duv { } {
    # Compile with the psl file
    vcom -pslfile ../src_tb/bcd_adder_assertions.psl ../../../../../src_vhd/bcd_adder.vhd -work work    -suppress 7033     -2002

}

proc check_psl {ERRNO NDIGITS} {

    formal compile -d bcd_adder -GERRNO=$ERRNO -GNDIGITS=$NDIGITS -work work

    formal verify
}

compile_duv

check_psl 0 4 
