
proc compile_duv { } {

    vcom ../../../../../src_vhd/timer_top.vhd -work work    -suppress 7033     -2002

}

proc check_sva {} {
    vlog ../src_tb/timer_top_assertions.sv  ../src_tb/timer_top_sv_wrapper.sv

    formal compile -d timer_top_sv_wrapper  -work work

    formal verify
}

compile_duv

check_sva 
