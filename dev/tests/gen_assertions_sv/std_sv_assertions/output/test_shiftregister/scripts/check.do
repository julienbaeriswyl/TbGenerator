
proc compile_duv { } {

    vcom ../../../../../src_vhd/shiftregister.vhd -work work    -suppress 7033     -2002

}

proc check_sva {DATASIZE} {
    vlog ../src_tb/shiftregister_assertions.sv  ../src_tb/shiftregister_sv_wrapper.sv

    formal compile -d shiftregister_sv_wrapper -GDATASIZE=$DATASIZE -work work

    formal verify
}

compile_duv

check_sva 8 
