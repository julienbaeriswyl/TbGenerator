/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : bcd_adder_sv_wrapper.sv
Author   : TbGenerator
Date     : 06.03.2019

Context  :

********************************************************************************
Description : This module is a wrapper that binds the DUV with the
              module containing the assertions

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   06.03.2019  TbGen      Initial version

*******************************************************************************/

// The wrapper has no port
module bcd_adder_sv_wrapper #(int ERRNO = 0, int NDIGITS = 4)();

    // Instantiation of the DUV
    bcd_adder#(ERRNO, NDIGITS) duv();

    // Binding of the DUV and the assertions module
    bind duv bcd_adder_assertions#(ERRNO, NDIGITS) binded(.*);

endmodule

/*
The following works as well

// The wrapper has the same interface as the component to verify
module bcd_adder_sv_wrapper #(int ERRNO = 0, int NDIGITS = 4)(
    input TODO__TypeNotCasted input0_i,
    input TODO__TypeNotCasted input1_i,
    output TODO__TypeNotCasted result_o,
    output logic[TODO__uncasted_left_member:0] hamming_o
);

    // Instantiation of the DUV
    bcd_adder#(ERRNO, NDIGITS) duv(.*);

    // Binding of the DUV and the assertions module
    bind duv bcd_adder_assertions#(ERRNO, NDIGITS) binded(.*);

endmodule
*/
