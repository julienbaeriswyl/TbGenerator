/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : bcd_adder_assertions.sv
Author   : TbGenerator
Date     : 06.03.2019

Context  :

********************************************************************************
Description : This module will contain the assertions for verifying
              bcd_adder

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   06.03.2019  TbGen      Initial version

*******************************************************************************/


// The module has the same interface as the component to verify except all
// ports are inputs
module bcd_adder_assertions #(int ERRNO = 0, int NDIGITS = 4)(
    input TODO__TypeNotCasted input0_i,
    input TODO__TypeNotCasted input1_i,
    input TODO__TypeNotCasted result_o,
    input logic[TODO__uncasted_left_member:0] hamming_o
    
);

    // TODO : Write your assertions

endmodule
