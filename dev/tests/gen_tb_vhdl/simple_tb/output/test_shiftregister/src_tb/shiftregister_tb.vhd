--------------------------------------------------------------------------------
-- HEIG-VD
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
--------------------------------------------------------------------------------
-- REDS Institute
-- Reconfigurable Embedded Digital Systems
--------------------------------------------------------------------------------
--
-- File     : shiftregister_tb.vhd
-- Author   : TbGenerator
-- Date     : 06.03.2019
--
-- Context  :
--
--------------------------------------------------------------------------------
-- Description : This module is a simple VHDL testbench.
--               It instanciates the DUV and proposes a TESTCASE generic to
--               select which test to start.
--
--------------------------------------------------------------------------------
-- Dependencies : -
--
--------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Person     Comments
-- 0.1   06.03.2019  TbGen      Initial version
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity shiftregister_tb is
    generic (
        TESTCASE : integer := 0;
        DATASIZE : integer := 8
    );
    
end shiftregister_tb;

architecture testbench of shiftregister_tb is

    constant CLK_PERIOD : time := 10 ns;

    signal clk_sti        : std_logic;
    signal rst_sti        : std_logic;
    signal mode_sti       : std_logic_vector(1 downto 0);
    signal load_value_sti : std_logic_vector(DATASIZE-1 downto 0);
    signal ser_in_msb_sti : std_logic;
    signal ser_in_lsb_sti : std_logic;
    signal value_obs      : std_logic_vector(DATASIZE-1 downto 0);
    
    signal sim_end_s : boolean := false;

    component shiftregister is
    generic (
        DATASIZE : integer := 8
    );
    port (
        clk_i        : in std_logic;
        rst_i        : in std_logic;
        mode_i       : in std_logic_vector(1 downto 0);
        load_value_i : in std_logic_vector(DATASIZE-1 downto 0);
        ser_in_msb_i : in std_logic;
        ser_in_lsb_i : in std_logic;
        value_o      : out std_logic_vector(DATASIZE-1 downto 0)
    );
    end component;
    

begin

    duv : shiftregister
    generic map (
        DATASIZE => DATASIZE
    )
    port map (
        clk_i        => clk_sti,
        rst_i        => rst_sti,
        mode_i       => mode_sti,
        load_value_i => load_value_sti,
        ser_in_msb_i => ser_in_msb_sti,
        ser_in_lsb_i => ser_in_lsb_sti,
        value_o      => value_obs
    );
    

    clk_proc : process is
    begin
        clk_sti <= '0';
        wait for CLK_PERIOD / 2;
        clk_sti <= '1';
        wait for CLK_PERIOD / 2;
        if sim_end_s then
            wait;
        end if;
    end process; -- clk_proc

    rst_proc : process is
    begin
        rst_sti <= '1';
        wait for 2 * CLK_PERIOD;
        rst_sti <= '0';
        wait;
    end process; -- rst_proc

    stimulus_proc: process is
    begin
        -- clk_sti        <= default_value;
        -- rst_sti        <= default_value;
        -- mode_sti       <= default_value;
        -- load_value_sti <= default_value;
        -- ser_in_msb_sti <= default_value;
        -- ser_in_lsb_sti <= default_value;
        

        -- do something
        case TESTCASE is
            when 0      => -- default testcase
            when others => report "Unsupported testcase : "
                                  & integer'image(TESTCASE)
                                  severity error;
        end case;

        -- end of simulation
        sim_end_s <= true;

        -- stop the process
        wait;

    end process; -- stimulus_proc

end testbench;
