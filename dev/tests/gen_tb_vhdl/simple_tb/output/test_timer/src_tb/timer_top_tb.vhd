--------------------------------------------------------------------------------
-- HEIG-VD
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
--------------------------------------------------------------------------------
-- REDS Institute
-- Reconfigurable Embedded Digital Systems
--------------------------------------------------------------------------------
--
-- File     : timer_top_tb.vhd
-- Author   : TbGenerator
-- Date     : 06.03.2019
--
-- Context  :
--
--------------------------------------------------------------------------------
-- Description : This module is a simple VHDL testbench.
--               It instanciates the DUV and proposes a TESTCASE generic to
--               select which test to start.
--
--------------------------------------------------------------------------------
-- Dependencies : -
--
--------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Person     Comments
-- 0.1   06.03.2019  TbGen      Initial version
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity timer_top_tb is
    generic (
        TESTCASE : integer := 0
    );
    
end timer_top_tb;

architecture testbench of timer_top_tb is

    constant CLK_PERIOD : time := 10 ns;

    signal clock_sti     : std_logic;
    signal nReset_sti    : std_logic;
    signal Mono_nDiv_sti : std_logic;
    signal en_div_sti    : std_logic;
    signal run_mono_sti  : std_logic;
    signal val_sti       : std_logic_vector(6 downto 0);
    signal done_obs      : std_logic;
    
    signal sim_end_s : boolean := false;

    component timer_top is
    port (
        clock_i     : in std_logic;
        nReset_i    : in std_logic;
        Mono_nDiv_i : in std_logic;
        en_div_i    : in std_logic;
        run_mono_i  : in std_logic;
        val_i       : in std_logic_vector(6 downto 0);
        done_o      : out std_logic
    );
    end component;
    

begin

    duv : timer_top
    port map (
        clock_i     => clock_sti,
        nReset_i    => nReset_sti,
        Mono_nDiv_i => Mono_nDiv_sti,
        en_div_i    => en_div_sti,
        run_mono_i  => run_mono_sti,
        val_i       => val_sti,
        done_o      => done_obs
    );
    

    clk_proc : process is
    begin
        clock_sti <= '0';
        wait for CLK_PERIOD / 2;
        clock_sti <= '1';
        wait for CLK_PERIOD / 2;
        if sim_end_s then
            wait;
        end if;
    end process; -- clk_proc

    rst_proc : process is
    begin
        clock_sti <= '1';
        wait for 2 * CLK_PERIOD;
        clock_sti <= '0';
        wait;
    end process; -- rst_proc

    stimulus_proc: process is
    begin
        -- clock_sti     <= default_value;
        -- nReset_sti    <= default_value;
        -- Mono_nDiv_sti <= default_value;
        -- en_div_sti    <= default_value;
        -- run_mono_sti  <= default_value;
        -- val_sti       <= default_value;
        

        -- do something
        case TESTCASE is
            when 0      => -- default testcase
            when others => report "Unsupported testcase : "
                                  & integer'image(TESTCASE)
                                  severity error;
        end case;

        -- end of simulation
        sim_end_s <= true;

        -- stop the process
        wait;

    end process; -- stimulus_proc

end testbench;
