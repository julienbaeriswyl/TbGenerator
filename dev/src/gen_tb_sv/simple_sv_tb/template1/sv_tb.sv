/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : ${entityname}_tb.sv
Author   : ${author}
Date     : ${date}

Context  :

********************************************************************************
Description : This module is a simple SystemVerilog testbench.
              It instanciates the DUV and proposes a testcase function.

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   ${date}  TbGen      Initial version

*******************************************************************************/


// The wrapper has the same interface as the component to verify
module ${entityname}_tb ${genericswithtestcase}();

    // Instantiation of the DUV
    ${entityname}${genericimpl} duv(.*);

    ${tbsignalsdeclaration}


    // clock generation
    always #5 ${clock} = ~${clock};

    // Clocking block
    ${clockingblock}


    task testcase0();
        $display("Let's start first test case");
        // TODO : assign default values to inputs

        // Let's reset the system
        cb.${reset} <= 1;
        ##1 cb.${reset} <= 0;

        // Let's wait a bit
        ##10;

        repeat (10) begin
            // Let's generate some stimuli
            // TODO : generate some stimuli

            ##1;
        end
    endtask



    // Programme lancé au démarrage de la simulation
    program TestSuite;
        initial begin
            if (testcase == 0)
                testcase0();
            else
                $display("Ach, test case not yet implemented");
            $display("done!");
            $stop;
        end
    endprogram

endmodule
