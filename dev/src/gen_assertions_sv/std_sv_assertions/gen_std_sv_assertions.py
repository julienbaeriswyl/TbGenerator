
import getopt
import sys
import os
import pprint
import time
from pathlib import Path

parserDir = os.path.dirname(__file__)+'/../../parsers'
sys.path.append(parserDir)
commonDir = os.path.dirname(__file__)+'/../../common'
sys.path.append(commonDir)

from vhdlentityparser import *
from common import *


# Check that we are using the correct Python version
if sys.version_info[0] < 3:
    raise "Must be using Python 3"


def generateSVA(inputFileName, destDir, templateDir, overwrite):
    print('Generating assertions structure')
    print('    File: {0}'.format(inputFileName))
    print('    Dest dir: {0}'.format(destDir))
    print('    Template: {0}'.format(templateDir))
    try:
        os.mkdir(destDir)
    except FileExistsError:
        None
        # print("The folder {0} already exists".format(destDir))

    try:
        os.mkdir(destDir + '/scripts')
    except FileExistsError:
        None
        # print("The folder {0} already exists".format(destDir + '/scripts'))

    try:
        os.mkdir(destDir + '/src_tb')
    except FileExistsError:
        None
        # print("The folder {0} already exists".format(destDir + '/src_tb'))

    try:
        os.mkdir(destDir + '/comp')
    except FileExistsError:
        None
        # print("The folder {0} already exists".format(destDir + '/comp'))

    Path(destDir + '/comp/.placeholder').touch()

    dir = os.path.dirname(__file__)
    realTemplateDir = os.path.join(dir, templateDir)
    print('    Real template: {0}'.format(realTemplateDir))

    entireFile = parseFile(inputFileName)
    entity = getEntity(entireFile)

#    pp = pprint.PrettyPrinter(depth=12)
#    pp.pprint(entity)

    portlist = ''
    for i in range(0, len(getPorts(entity))):
        port = getPorts(entity)[i]
        portlist += '{0} {1} {2}'.format(getSvPortDirection(port), getSvPortType(port), getPortName(port))
        if (not (i == len(getPorts(entity)) - 1)):
            portlist += ','
            portlist += '\n'

    inputportlist = ''
    for i in range(0, len(getPorts(entity))):
        port = getPorts(entity)[i]
        inputportlist += 'input {0} {1}'.format(getSvPortType(port), getPortName(port))
        if (not (i == len(getPorts(entity)) - 1)):
            inputportlist += ','
        inputportlist += '\n'

    defaultGenerics = ''
    for gen in getGenerics(entity):
        defaultGenerics += str(getGenericValue(gen)) + ' '

    entityRelativePath = os.path.relpath(inputFileName, destDir + '/scripts')

    replacements = {'entityname' : getName(entity),
            'author' : 'TbGenerator',
            'date' : time.strftime("%d.%m.%Y"),
            'generics' : getSvGenerics(entity),
            'portlist' : portlist,
            'allinputportlist' : inputportlist,
            'genericimpl' : getSvGenericsShort(entity),
            'duvfilerelativepath' : entityRelativePath,
            'defaultgenerics' : defaultGenerics,
            'genericstcllist' : getGenericsTclList(entity),
            'genericstcldollarlist' : getGenericsTclDollarList(entity),
            'genericstclgenericlist' : getGenericsTclGenericList(entity)
            }


    generateFile(realTemplateDir + '/sv_wrapper.sv',
                 destDir+'/src_tb/{0}_sv_wrapper.sv'.format(getName(entity)),
                 replacements, overwrite)
    generateFile(realTemplateDir + '/assertions.sv',
                 destDir+'/src_tb/{0}_assertions.sv'.format(getName(entity)),
                 replacements, overwrite)
    generateFile(realTemplateDir + '/check.do',
                 destDir+'/scripts/check.do',
                 replacements, overwrite)


def printhelp():
    print ('gen_std_sv_assertions.py -i <inputfile> -d <destdir> -t <templatedir>')
    print ('    -h will show this help')
    print ('    -f forces overwrite of existing files. Use with care.')

def main(argv):
    inputFileName = ''
    destDir    = './'
    templateDir = 'template1'
    overwrite = False

    try:
       opts, args = getopt.getopt(argv,"hfi:t:d:",["ifile","templatedir","destdir"])
    except getopt.GetoptError:
        printhelp()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            printhelp()
            sys.exit()
        elif opt in ("-f"):
            overwrite = True
        elif opt in ("-i", "--ifile"):
            inputFileName = arg
        elif opt in ("-d", "--destdir"):
            destDir = arg
        elif opt in ("-t", "--templatedir"):
            templateDir = arg

    generateSVA(inputFileName, destDir, templateDir, overwrite)


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1:])
