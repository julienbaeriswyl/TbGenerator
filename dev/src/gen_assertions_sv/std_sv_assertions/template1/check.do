
proc compile_duv { } {

    vcom ${duvfilerelativepath} -work work    -suppress 7033     -2002

}

proc check_sva {${genericstcllist}} {
    vlog ../src_tb/${entityname}_assertions.sv  ../src_tb/${entityname}_sv_wrapper.sv

    formal compile -d ${entityname}_sv_wrapper ${genericstclgenericlist} -work work

    formal verify
}

compile_duv

check_sva ${defaultgenerics}
