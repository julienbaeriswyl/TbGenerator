/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : ${entityname}_sv_wrapper.sv
Author   : ${author}
Date     : ${date}

Context  :

********************************************************************************
Description : This module is a wrapper that binds the DUV with the
              module containing the assertions

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   ${date}  TbGen      Initial version

*******************************************************************************/

// The wrapper has no port
module ${entityname}_sv_wrapper ${generics}();

    // Instantiation of the DUV
    ${entityname}${genericimpl} duv();

    // Binding of the DUV and the assertions module
    bind duv ${entityname}_assertions${genericimpl} binded(.*);

endmodule

/*
The following works as well

// The wrapper has the same interface as the component to verify
module ${entityname}_sv_wrapper ${generics}(
    ${portlist}
);

    // Instantiation of the DUV
    ${entityname}${genericimpl} duv(.*);

    // Binding of the DUV and the assertions module
    bind duv ${entityname}_assertions${genericimpl} binded(.*);

endmodule
*/
