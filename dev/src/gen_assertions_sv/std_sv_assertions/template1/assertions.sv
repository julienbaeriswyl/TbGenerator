/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : ${entityname}_assertions.sv
Author   : ${author}
Date     : ${date}

Context  :

********************************************************************************
Description : This module will contain the assertions for verifying
              ${entityname}

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   ${date}  TbGen      Initial version

*******************************************************************************/


// The module has the same interface as the component to verify except all
// ports are inputs
module ${entityname}_assertions ${generics}(
    ${allinputportlist}
);

    // TODO : Write your assertions

endmodule
