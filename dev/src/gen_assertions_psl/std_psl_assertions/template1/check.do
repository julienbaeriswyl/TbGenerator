
proc compile_duv { } {
    # Compile with the psl file
    vcom -pslfile ../src_tb/${entityname}_assertions.psl ${duvfilerelativepath} -work work    -suppress 7033     -2002

}

proc check_psl {${genericstcllist}} {

    formal compile -d ${entityname} ${genericstclgenericlist} -work work

    formal verify
}

compile_duv

check_psl ${defaultgenerics}
