#
# This parser does not parses the types. It keeps only a string for a type.
# To be used for VHDL generation, not SystemVerilog generation
#

import re
import pprint

# A PrettyPrinter for debugging purpose
pp = pprint.PrettyPrinter(depth=12)



# Remove all VHDL comments in a string
# This function is applied before the regexps
def stripVhdlComments(code):
    code = str(code)
    return re.sub('--.*\n', '\n', code)



portdecl = re.compile(r"""
 ([^:]*)         # Port name : group(1)
 \s*             # Spaces
 \:              # separation
 \s*             # Spaces
 (\w+)           # Port direction : group(2)
 \s+             # At least one space
 ([^;,:]*)       # Type of the port : group(3)
""", re.VERBOSE)

# A colon item is anything except a colon
colonitemdecl = r'\s*(\w+[^,]*?)\s*'

portnamelist = re.compile(
 '(?:' +          # Optional list of items
 colonitemdecl +
 ',)*' +
 colonitemdecl    # The required list item
, re.VERBOSE)


portsdeclraw = re.compile(r"""
 port            # Start with the keyword
 \s*             # Spaces
 \(              # Opening parenthesis
 (.*)            # The list of ports : group(2)
 \);             # Closing parenthesis
 \s*             # Spaces
 \Z              # End of string
 """, re.VERBOSE | re.DOTALL)

genericsdeclraw = re.compile(r"""
 generic         # Start with the keyword
 \s*             # Spaces
 \(              # Opening parenthesis
 (.*)            # The list of generics : group(1)
 \);             # Closing parenthesis
 \s*             # Spaces
 \Z              # End of string
""", re.VERBOSE | re.DOTALL)


genericdecl = re.compile(r"""
 \s*             # Strip spaces
 (\w+)           # Generic name : group(1)
 \s*             # Spaces
 \:              # :
 \s*             # Spaces
 ([^:]*)         # The type : group(2)
 \s*             # Spaces
 (?:             # Optional default value
   \:=           #
   \s*           #
   (\w*)         # Default value : group(3)
 )?              #
 """, re.VERBOSE | re.DOTALL)

entityinside = re.compile(r"""
 \s*                 # Look for a first generic
 (generic\s*\(.*\);) # Get the generic declaration : group(1)
 \s*                 # Intermediate spaces
 (port\s*\(.*\);)    # Get the ports declaration : group(2)
 \s*                 # Trailing spaces
""", re.VERBOSE | re.DOTALL)

entityinsideonlyport = re.compile(r"""
 \s*                 # Look for a first generic
 (port\s*\(.*\);)    # Get the ports declaration : group(2)
 \s*                 # Trailing spaces
""", re.VERBOSE | re.DOTALL)


entitydecl = re.compile(r"""
 .*             # We look for the first entity
 \bentity\s+    # Entity followed by at least a space
 (\w+)          # Entity name : group(1)
 \s+is\s+       # Followed by at least a space, then is
 (.*)           # Content of the entity : group(2)
 \s+\bend\b\s*  # End of entity
 (?:\w+)?\s*;   # With optional entity name
 """, re.VERBOSE | re.DOTALL)

splitdecl = re.compile(r"""
 .*             # We look for the first entity
 (\bentity\s+    # Entity followed by at least a space
 \w+          # Entity name : group(1)
 \s+is\s+       # Followed by at least a space, then is
 .*           # Content of the entity : group(2)
 \s+\bend\b\s*  # End of entity
 (?:\w+)?\s*;)   # With optional entity name
 \s*
 architecture\s+    # Entity followed by at least a space
 (\w+)          # Entity name : group(1)
 \s+of\s+       # Followed by at least a space, then is
 (\w+)          # Entity name : group(1)
 \s+is\s+       # Followed by at least a space, then is
 .*           # Content of the entity : group(2)
 """, re.VERBOSE | re.DOTALL)

# Only to find if there is at least one generic in the entity
findGeneric = re.compile(r'.*\bgeneric\b.*', re.DOTALL)

# Only to find if there are ports in the entity
findPort = re.compile(r'.*\bport\b.*', re.DOTALL)

def parseString(vhdlSource):

    # Declarations
    allContent = {}
    entity = {}
    allPorts = []
    allGenerics = []


    entityMatch = entitydecl.search(stripVhdlComments(vhdlSource))

    firstpart = splitdecl.search(stripVhdlComments(vhdlSource))
    if (firstpart):
        entityMatch = entitydecl.search(firstpart.group(1))

    if (entityMatch):
        entity['name'] = entityMatch.group(1)

        # Get the inside part of the entity
        # Actually it is wrong now. It would get the architecture as well...
        entityIn = entityMatch.group(2)

        # Try to find the generic keyword
        hasGenerics = findGeneric.match(entityIn)

        # Try to find the port keyword
        hasPorts = findPort.match(entityIn)

        if (hasGenerics):
            # If only a generic part, then select the entire entity
            genericsPart = entityIn

        if (hasPorts and not hasGenerics):
            inside = entityinsideonlyport.match(entityIn)
            if (inside):
                portsPart = inside.group(1)

        if (hasPorts and hasGenerics):
            # If both generic and ports, then split into two parts
            inside = entityinside.match(entityIn)
            if (inside):
                genericsPart = inside.group(1)
                portsPart = inside.group(2)

        if hasPorts:
            # Analyze the ports part
            portslistmatch = portsdeclraw.match(portsPart)
            ports = portslistmatch.group(1).split(';')
            for portString in ports:
                # For each single port:
                #print(portString)
                portMatch = portdecl.match(portString)
                # For the direction, store the uppercase string
                portDirection = portMatch.group(2).upper()
                portType = portMatch.group(3).strip()
                allPortName = portMatch.group(1)
                allp = allPortName.split(',')
                portsnamesMatch = portnamelist.match(allPortName)
                for pName in allp:
                    portName = pName.strip()
                    allPorts.append({'name' : portName, 'type' : portType, 'direction' : portDirection})
                    #print('    Port: "{0}" : "{1}" "{2}"'.format(portName, portDirection, portType))

        if hasGenerics:
            # Analyze the generics part
            genericslistmatch = genericsdeclraw.match(genericsPart)
            generics = genericslistmatch.group(1).split(';')
            for genericString in generics:
                # For each single generic
                genericMatch = genericdecl.match(genericString)
                genericName = genericMatch.group(1).strip()
                genericType = genericMatch.group(2).strip()
                genericValue = ''
                if (genericMatch.group(3)):
                    genericValue = genericMatch.group(3).strip()
                allGenerics.append({'name' : genericName, 'type' : genericType, 'value' : genericValue})
                #print('    Generic: "{0}" : "{1}" := "{2}"'.format(genericName, genericType, genericValue))
    # Fill the entity
    entity['ports']      = allPorts
    entity['generics']   = allGenerics

    # Fill the entire content. Could be extended in the future
    allContent['entity'] = entity

    return allContent


# Parses the file and returns its analyzed content
def parseFile(fileName):
    # Open the file
    vhdlFile = open(fileName)
    sourceCode = vhdlFile.read()
    return parseString(sourceCode)

def getEntity(r):
    return r['entity']

def getName(e):
    return e['name']

def getGenerics(e):
    return e['generics']

def getPorts(e):
    return e['ports']

def getPortDirection(p):
    return p['direction']

def getPortName(p):
    return p['name']

def getPortType(p):
    return p['type']

def getGenericName(g):
    return g['name']

def getGenericType(g):
    return g['type']

def getGenericValue(g):
    return g['value']

def getVhdlPortDirection(p):
    if (p['direction'] == 'IN'):
        return 'in'
    if (p['direction'] == 'OUT'):
        return 'out'
    if (p['direction'] == 'INOUT'):
        return 'inout'
    return 'unkownDirection'

def isInputPort(p):
    return p['direction'] ==  'IN'

def isOutputPort(p):
    return p['direction'] ==  'OUT'


def getInputPorts(e):
    ports = []
    for port in getPorts(e):
        if (isInputPort(port)):
            ports.append(port)
    return ports

def getOutputPorts(e):
    ports = []
    for port in getPorts(e):
        if (isOutputPort(port)):
            ports.append(port)
    return ports


def isTypeLogic(t):
    return t['type'] == 'std_logic'


# Try to find if a port is a clock.
# Should contain "cl", and "k" in it.
def isClock(p):
    match = re.search("cl\W*k\W*", getPortName(p).lower())
    if match:
        return True
    return False

# Try to find if a port is a reset.
# Should contain "r", "s" and "t" in it.
def isReset(p):
    match = re.search("r\W*s\W*t\W*", getPortName(p).lower())
    if match:
        return True
    return False

# Returns a space-separated list of generics
def getGenericsTclList(e):
    content = ''
    for i in range(0, len(getGenerics(e))):
        gen = getGenerics(e)[i]
        content += '{0}'.format(getGenericName(gen))
        if (not (i == len(getGenerics(e)) - 1)):
            content += ' '
    return content

# Returns a space-separated list of generics where every generic as a $ prefix
def getGenericsTclDollarList(e):
    content = ''
    for i in range(0, len(getGenerics(e))):
        gen = getGenerics(e)[i]
        content += '${0}'.format(getGenericName(gen))
        if (not (i == len(getGenerics(e)) - 1)):
            content += ' '
    return content

# Returns a space-separated list of generics for starting the simulation.
# For instance "-Gabcd=abcd -Gefgh=efgh"
def getGenericsTclGenericList(e):
    content = ''
    for i in range(0, len(getGenerics(e))):
        gen = getGenerics(e)[i]
        content += '-G{0}=${0}'.format(getGenericName(gen))
        if (not (i == len(getGenerics(e)) - 1)):
            content += ' '
    return content
