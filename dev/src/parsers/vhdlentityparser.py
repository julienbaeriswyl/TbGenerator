#
# Python3 script
# Requires hdlConvertor
#     It can be installed with pip3
#
# This parser is meant to be used for SystemVerilog generation


import hdlConvertor
import pprint
import re

def parseFile(fileName):
    return hdlConvertor.parse(fileName, "vhdl")

def getEntity(r):
    return r['entities'][0]

def getGenerics(e):
    return e['generics']

def getPorts(e):
    return e['ports']

def getGenericName(g):
    return g['name']

def getGenericType(g):
    return g['type']['literal']['value']

def getGenericValue(g):
    return g['value']['literal']['value']

def hasDefaultValue(g):
    try:
        getGenericValue(g)
        return True
    except:
        return False

def getSvType(t):
    theType = t['literal']['value']
    if (theType == 'integer'):
        return 'int'
    return t['literal']['value']

def getSvGenerics(e):
    content = ''
    if (len(getGenerics(e)) == 0):
        return content
    content = '#('
    for i in range(0, len(getGenerics(e))):
        gen = getGenerics(e)[i]
        default = ''
        if (hasDefaultValue(gen)):
            default = ' = {0}'.format(getGenericValue(gen))
        content += '{0} {1}{2}'.format(getSvType(getType(gen)), getGenericName(gen), default)
        if (not (i == len(getGenerics(e)) - 1)):
            content += ', '
    content += ')'
    return content

def getSvGenericsWithTestCase(e):
    content = ''
    if (len(getGenerics(e)) == 0):
        return content
    content = '#('
    for i in range(0, len(getGenerics(e))):
        gen = getGenerics(e)[i]
        default = ''
        if (hasDefaultValue(gen)):
            default = ' = {0}'.format(getGenericValue(gen))
        content += '{0} {1}{2}'.format(getSvType(getType(gen)), getGenericName(gen), default)
        content += ', '
    content += 'int TESTCASE = 0'
    content += ')'
    return content

def getSvGenericsShort(e):
    content = ''
    if (len(getGenerics(e)) == 0):
        return content
    content = '#('
    for i in range(0, len(getGenerics(e))):
        gen = getGenerics(e)[i]
        content += '{0}'.format(getGenericName(gen))
        if (not (i == len(getGenerics(e)) - 1)):
            content += ', '
    content += ')'
    return content


def getGenericsTclList(e):
    content = ''
    for i in range(0, len(getGenerics(e))):
        gen = getGenerics(e)[i]
        content += '{0}'.format(getGenericName(gen))
        if (not (i == len(getGenerics(e)) - 1)):
            content += ' '
    return content


def getGenericsTclDollarList(e):
    content = ''
    for i in range(0, len(getGenerics(e))):
        gen = getGenerics(e)[i]
        content += '${0}'.format(getGenericName(gen))
        if (not (i == len(getGenerics(e)) - 1)):
            content += ' '
    return content


def getGenericsTclGenericList(e):
    content = ''
    for i in range(0, len(getGenerics(e))):
        gen = getGenerics(e)[i]
        content += '-G{0}=${0}'.format(getGenericName(gen))
        if (not (i == len(getGenerics(e)) - 1)):
            content += ' '
    return content

def getName(e):
    return e['name']

def getType(e):
    return e['type']

def getPortDirection(p):
    return p['direction']

def getPortName(p):
    return p['variable']['name']

def getPortType(p):
    return p['variable']['type']

def getSvPortDirection(p):
    if (p['direction'] == 'IN'):
        return 'input'
    if (p['direction'] == 'OUT'):
        return 'output'
    return 'unkownDirection'

def isInputPort(p):
    return p['direction'] ==  'IN'

def isOutputPort(p):
    return p['direction'] ==  'OUT'

def getInputPorts(e):
    ports = []
    for port in getPorts(e):
        if (isInputPort(port)):
            ports.append(port)
    return ports

def getOutputPorts(e):
    ports = []
    for port in getPorts(e):
        if (isOutputPort(port)):
            ports.append(port)
    return ports

def isClock(p):
    match = re.search("cl\W*k\W*", getPortName(p).lower())
    if match:
        return True
    return False

def isReset(p):
    match = re.search("r\W*s\W*t\W*", getPortName(p).lower())
    if match:
        return True
    return False

def isTypeLogic(t):
    if 'literal' in t:
        return t['literal']['value'] == 'std_logic'
    return False


def isTypeLogicVector(t):
    if not ('binOperator' in t):
        return False
    return t['binOperator']['op0']['literal']['value'] == 'std_logic_vector'

def getRange(t):
    r = {}
    try:
        r['dir'] = t['binOperator']['op1']['binOperator']['operator']
        try:
            r['rLeft'] = t['binOperator']['op1']['binOperator']['op0']['literal']['value']
        except:
            try:
                if (t['binOperator']['op1']['binOperator']['op0']['binOperator']['operator'] == 'SUB'):
                    op = t['binOperator']['op1']['binOperator']['op0']['binOperator']
                    r['rLeft'] = '{0} - {1}'.format(op['op0']['literal']['value'], op['op1']['literal']['value'])
                else:
                    r['rLeft'] = 'TODO__uncasted_left_member'
            except:
                r = 'unvalid'
                print('can not cast left member')
                pp = pprint.PrettyPrinter(depth=12)
                pp.pprint(t)
                return r
        r['rRight'] = t['binOperator']['op1']['binOperator']['op1']['literal']['value']
    except:
        r = 'unvalid'
    return r

def isValid(r):
    return not (r == 'unvalid')

def typeToVHDL(t):
    print('To be implemented')

def typeToSV(t):
    print('To be implemented')

def getSvPortType(p):
    t = getPortType(p)
    if (isTypeLogic(t)):
        return 'logic'
    if (isTypeLogicVector(t)):
        if (isValid(getRange(t))):
            return 'logic[{0}:{1}]'.format(getRange(t)['rLeft'],getRange(t)['rRight'])
    return 'TODO__TypeNotCasted'
