This project aims at offering automatic generation of various verification
methodologies for HDL designs.

The following are being under development:
- Simple VHDL testbench
- Simple SystemVerilog testbench
- Assertion-based verification in SystemVerilog

It requires Python3

Documentation can be read here: http://tbgenerator.readthedocs.io/en/latest/
