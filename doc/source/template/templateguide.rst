
Welcome to TbGenerator's template writer guide
==============================================

The scripts are based on a very simple principle. Template files are used to
generate the output based on what the Python scripts prepare as data. The
templates embed some patterns that are replaced by the scripts.

These patterns have the following form::

    ${patternid}

patternid is simply an identifier for which the scripts have to prepare a string
to use as replacement of the pattern. Therefore, the scripts and the templates
shall agree on the dictionary of pattern ids. The choice of this ${} was made
in order to be able to cope with VHDL and SystemVerilog templates. SystemVerilog
uses the $, but only imediately before an alphanumeric character.

The scripts are able to correctly deal with spaces or tabs present at the
beginning of the line. For instance, if the template has a pattern after 4
spaces, and if the replacement string contains new lines, then 4 spaces will be
added at the beginning of each line, so as to respect the indentation of both
the template and the replacement string.

Example template::

    someCode
        ${insertid}
    someMoreCode

Replacement string::

    someStatement
        someIndented
    someNot

Result::

    someCode
        someStatement
            someIndented
        someNot
    someModeCode


Each script exposes some pattern id replacements. Therefore writing a template
for existing scripts requires to use the available pattern ids, and only the
available ones.

The next sections detail the available ids for each specific script.

Simple VHDL testbench
---------------------


.. csv-table:: IDs and their meaning
   :header: "ID", "Description"
   :widths: 20, 40

    entityname , name of the entity
    author , Name of the author. Currently : TbGenerator
    date , "Current date, in the format dd.mm.YYYY"
    signalsdeclaration , declaration of all testbench signals
    componentdeclaration , declaration of the component (DUV)
    componentinstantiation , instantiation of the component (DUV)
    duvfilerelativepath , relative path to the file containing the entity
    defaultstimuli , assignation of default values to stimuli
    tbclock , name of the clock signal
    tbreset , name of the reset signal
    genericstcllist , space-separated list of generic parameters
    genericstcldollarlist , "space-separated list of generic parameters, each one being preceded by dollar $"
    genericstclgenericlist , "space-separated list of generic parameters definition for starting the simulation. Like -GPARAM=PARAM"
    entitydirpath , "relative path to the entity to be tested, from comp folder"
    tbgenerics , "list of generics to be put in the testbench"
    defaultgenerics , "default value for generics in the sim.do file"
    genericarguments , "list of generic arguments for the sim.do file. Like $2 $3 $4"



Simple SystemVerilog testbench
------------------------------


SystemVerilog assertions
------------------------

PSL assertions
--------------
