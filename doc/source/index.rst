.. TbGenerator documentation master file, created by
   sphinx-quickstart on Fri Jan 26 17:41:00 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TbGenerator's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   User guide <user/userguide>
   Developer guide <developer/developerguide>
   Template writer guide <template/templateguide>
   Examples <examples/examples>

TbGenerator is a set of python3 scripts able to generate skeleton for VHDL and SystemVerilog testbenches.
It can also generate skeletons for SystemVerilog assertions and PSL assertions.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
