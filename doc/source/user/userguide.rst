
Welcome to TbGenerator's user guide
===================================

TbGenerator consists in a set of scripts for generating various digital systems
verification related systems, from VHDL testbenches up to assertions-ready
modules.


Installation
------------

You first need python3 to be installed.
Pip will then allow to easily install hdlConvertor.
On Linux, install pip::

    sudo apt-get install python3-pip

Then, hdlConvertor::

    sudo pip3 install hdlConvertor

Everything should go well at this point.




VHDL testbench
--------------

For generating a testbench, the script needs three parameters:
    * The VHDL file containing the entity to scan
    * The destination folder where the files will be generated
    * The folder containing the templates

It can be quite dangerous to overwrite existing files through running the script
on an existing directory. By default existing files won't be overwritten, but if
you wish to do so, then run it with the -f option.

Here is the result of running the script with -h ::

    python3 gen_simple_vhdl_tb.py -i <inputfile> -d <destdir> -t <templatedir>
        -h will show this help
        -f forces overwrite of existing files. Use with care.

Currently, only 2 templates exist: template1 for synchronous designs, and template_combi1
 for combinatorial ones. It is the responsibility of the user to choose the correct one.



SystemVerilog testbench
-----------------------

For generating a SystemVerilog testbench, the script needs three parameters:
    * The VHDL file containing the entity to scan
    * The destination folder where the files will be generated
    * The folder containing the templates

It can be quite dangerous to overwrite existing files through running the script
on an existing directory. By default existing files won't be overwritten, but if
you wish to do so, then run it with the -f option.

Here is the result of running the script with -h ::

    python3 gen_simple_sv_tb.py -i <inputfile> -d <destdir> [-t <templatedir>]
        -h will show this help
        -f forces overwrite of existing files. Use with care.

There is currently only a single template: template1. Therefore, there is no
need to specify the template directory.


SystemVerilog assertions formal verification
-------------------------------------------------

For generating a SystemVerilog assertion setup, the script needs three parameters:
    * The VHDL file containing the entity to scan
    * The destination folder where the files will be generated
    * The folder containing the templates

It can be quite dangerous to overwrite existing files through running the script
on an existing directory. By default existing files won't be overwritten, but if
you wish to do so, then run it with the -f option.

Here is the result of running the script with -h ::

    python3 gen_std_sv_assertions.py -i <inputfile> -d <destdir> [-t <templatedir>]
        -h will show this help
        -f forces overwrite of existing files. Use with care.

There is currently only a single template: template1. Therefore, there is no
need to specify the template directory.

PSL assertions formal verification
----------------------------------

For generating a PSL assertion setup, the script needs three parameters:
    * The VHDL file containing the entity to scan
    * The destination folder where the files will be generated
    * The folder containing the templates

It can be quite dangerous to overwrite existing files through running the script
on an existing directory. By default existing files won't be overwritten, but if
you wish to do so, then run it with the -f option.

Here is the result of running the script with -h ::

    python3 gen_std_psl_assertions.py -i <inputfile> -d <destdir> [-t <templatedir>]
        -h will show this help
        -f forces overwrite of existing files. Use with care.

There is currently only a single template: template1. Therefore, there is no
need to specify the template directory.
